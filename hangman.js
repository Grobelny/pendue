var bonmot = "Après la mort, le médecin.";
bonmot = bonmot.toUpperCase();

var longueur = bonmot.length;
var perdue = 0;

var yes = new Audio("yes.wav");
var no = new Audio("no.wav");

var bonmot1 = "";

for (i=0; i<longueur; i++)
{
    if (bonmot.charAt(i)==" ") bonmot1 = bonmot1 + " ";
    else bonmot1 = bonmot1 + "-";
}

function givemebonmot()
{
    document.getElementById("bord").innerHTML = bonmot1;
}

window.onload = start;

var litery = new Array(35);

litery[0] = "A";
litery[1] = "À";
litery[2] = "B";
litery[3] = "C";
litery[4] = "Ç";
litery[5] = "D";
litery[6] = "E";
litery[7] = "É";
litery[8] = "È";
litery[9] = "F";
litery[10] = "G";
litery[11] = "H";
litery[12] = "I";
litery[13] = "J";
litery[14] = "K";
litery[15] = "L";
litery[16] = "M";
litery[17] = "N";
litery[18] = "O";
litery[19] = "P";
litery[20] = "Q";
litery[21] = "R";
litery[22] = "S";
litery[23] = "T";
litery[24] = "U";
litery[25] = "V";
litery[26] = "W";
litery[27] = "X";
litery[28] = "Y";
litery[29] = "Z";
litery[30] = ",";
litery[31] = ".";
litery[32] = ";";
litery[33] = "!";
litery[34] = "?";


function start()
{

    var out_of_div ="";

    for (i=0; i<=34; i++)
    {
        var element = "lit" + i;
        out_of_div = out_of_div + '<div class="lettre" onclick="check_it('+i+')" id="'+element+'">'+litery[i]+'</div>';

        if ((i+1) % 7 ==0) out_of_div = out_of_div + '<div style="clear:both;"></div>';
    }

    document.getElementById("alfabet").innerHTML = out_of_div;

    givemebonmot();
}

String.prototype.mettreSign = function(place, sign)
{
    if (place > this.length - 1) return this.toString();
    else return this.substr(0, place) + sign + this.substr(place+1);
}

function check_it(nr)
{
    var coup = false;

    for(i=0; i<longueur; i++)
    {
        if (bonmot.charAt(i) == litery[nr])
        {
            bonmot1 = bonmot1.mettreSign(i,litery[nr]);
            coup = true;
        }
    }

    if(coup == true)
    {   
        yes.play();
        var element = "lit" + nr;
        document.getElementById(element).style.background = "#33BBFF";
        document.getElementById(element).style.color = "aliceblue";
        document.getElementById(element).style.border = "3px solid aliceblue";
        document.getElementById(element).style.cursor = "default";

        givemebonmot();
    }
    else
    { 
        no.play();
        var element = "lit" + nr;
        document.getElementById(element).style.background = "aliceblue";
        document.getElementById(element).style.color = "#33BBFF";
        document.getElementById(element).style.border = "3px solid #33BBFF";
        document.getElementById(element).style.cursor = "default";
        document.getElementById(element).setAttribute("onclick",";");

        //perdue
        perdue++;
        var images = "img/s"+ perdue + ".jpg";
        document.getElementById("hanging").innerHTML = '<img src="'+images+'" alt="" />';
    }

    //gagner
    if (bonmot == bonmot1)
    document.getElementById("alfabet").innerHTML = "Youupi Youp! You Win!: "+bonmot+
    '<br /><br /><span class="reset" onclick="location.reload()">ENCORE UNE FOIS?</span>';

    //pendue
    if (perdue>=9)
    document.getElementById("alfabet").innerHTML = "You Loose! Good Answer Is: "+bonmot+
    '<br /><br /><span class="reset" onclick="location.reload()">ENCORE UNE FOIS?</span>';

	
}